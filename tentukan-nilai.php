<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tentukan Nilai</title>
</head>

<body>
    <h1>1. Tentukan Nilai</h1>
    <?php
    function tentukan_nilai($number)
    {
        if ($number >= 85 && $number <= 100) {
            echo "Sangat Baik <br>";
        } else if ($number >= 70 && $number < 85) {
            echo "Baik <br>";
        } else if ($number >= 60 && $number < 70) {
            echo "Cukup <br>";
        } else {
            echo "Kurang <br>";
        }
    }

    //TEST CASES
    echo tentukan_nilai(98); //Sangat Baik
    echo tentukan_nilai(76); //Baik
    echo tentukan_nilai(67); //Cukup
    echo tentukan_nilai(43); //Kurang
    ?>

    <h1>2. Ubah Huruf</h1>
    <?php
    function ubah_huruf($string)
    {
        for ($i = 0; $i < strlen($string); $i++) {
            $strrep[$i] = chr(ord($string[$i]) + 1);
        }
        echo implode($strrep) . "<br>";
    }

    //TEST CASES
    echo ubah_huruf('wow'); // xpx
    echo ubah_huruf('developer'); // efwfmpqfs
    echo ubah_huruf('laravel'); // mbsbwfm
    echo ubah_huruf('keren'); // lfsfo
    echo ubah_huruf('semangat'); // tfnbohbu

    ?>

    <h1>3. Tukar Besar Kecil</h1>

    <?php
    function tukar_besar_kecil($string)
    {
        $abjad = implode(range("a", "z"));
        $kata_baru = "";
        // var_dump($abjad);
        for ($i = 0; $i < strlen($string); $i++) {
            $kata = mb_strpos($abjad, $string[$i]);
            if ($kata == null) {
                $kata_baru .= strtolower($string[$i]);
            } else {
                $kata_baru .= strtoupper($string[$i]);
            }
        }
        return $kata_baru . "<br>";
    }

    // TEST CASES
    echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
    echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
    echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
    echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
    echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

    ?>



</body>

</html>